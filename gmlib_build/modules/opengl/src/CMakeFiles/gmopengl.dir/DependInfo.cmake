# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/bufferobjects/gmindexbufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/bufferobjects/gmindexbufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/bufferobjects/gmtexturebufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/bufferobjects/gmtexturebufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/bufferobjects/gmuniformbufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/bufferobjects/gmuniformbufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/bufferobjects/gmvertexbufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/bufferobjects/gmvertexbufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmbufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmbufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmframebufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmframebufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmopenglmanager.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmopenglmanager.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmprogram.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmprogram.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmprogrampipeline.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmprogrampipeline.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmrenderbufferobject.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmrenderbufferobject.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmshader.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/gmtexture.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/gmtexture.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/shaders/gmcomputeshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/shaders/gmcomputeshader.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/shaders/gmfragmentshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/shaders/gmfragmentshader.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/shaders/gmgeometryshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/shaders/gmgeometryshader.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/shaders/gmtesscontrolshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/shaders/gmtesscontrolshader.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/shaders/gmtessevaluationshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/shaders/gmtessevaluationshader.cpp.obj"
  "C:/STE6245/gmlib/gmlib/modules/opengl/src/shaders/gmvertexshader.cpp" "C:/STE6245/gmlib_build/modules/opengl/src/CMakeFiles/gmopengl.dir/shaders/gmvertexshader.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GM_GL_DEBUG"
  "GM_STREAM"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "include"
  "C:/Program Files (x86)/glew/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
