# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/STE6245/googletest-master/googletest/src/gtest_main.cc" "C:/STE6245/gtest_build/googlemock/gtest/CMakeFiles/gtest_main.dir/src/gtest_main.cc.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/STE6245/googletest-master/googletest/include"
  "C:/STE6245/googletest-master/googletest"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/STE6245/gtest_build/googlemock/gtest/CMakeFiles/gtest.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
