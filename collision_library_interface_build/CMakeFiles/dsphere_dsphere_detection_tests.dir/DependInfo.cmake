# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/STE6245/collision_library_interface/collision_library_interface/unittests/dsphere_dsphere_detection_tests.cc" "C:/STE6245/collision_library_interface_build/CMakeFiles/dsphere_dsphere_detection_tests.dir/unittests/dsphere_dsphere_detection_tests.cc.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GM_GL_DEBUG"
  "GM_STREAM"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "C:/STE6245/gmlib_build/include"
  "C:/Program Files (x86)/glew/include"
  "C:/STE6245/collision_library_interface/collision_library_interface/include"
  "C:/Program Files (x86)/googletest-distribution/include"
  "C:/STE6245/collision_library_template/collision_library_template/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
